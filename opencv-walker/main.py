import cv2 as cv

img = cv.imread('resources/input/dummy/002.jpeg')

retVal, threshold = cv.threshold(img, 20, 155, cv.THRESH_BINARY)
grayImg = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
retVal, grayThreshold = cv.threshold(grayImg, 20, 155, cv.THRESH_BINARY)

adaptiveGauss = cv.adaptiveThreshold(grayImg, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 115, 1)
adaptiveMean = cv.adaptiveThreshold(grayImg, 255, cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY, 115, 1)

cv.imshow('input image', img)
# cv.waitKey(0)
cv.imshow('gray image', grayImg)
cv.imshow('threshold image', threshold)
cv.imshow('gray threshold', grayThreshold)
cv.imshow('adaptiveGauss', adaptiveGauss)
cv.imshow('adaptiveMean', adaptiveMean)

cv.waitKey(0)


cv.destroyAllWindows()

