import cv2 as cv
# import time

# settings
# DELAY_S = 0.200
FILE_NAME = 'resources/input/dummy/003.jpeg'

# ADAPTIVE_METHOD = cv.ADAPTIVE_THRESH_GAUSSIAN_C
# ADAPTIVE_METHOD = cv.ADAPTIVE_THRESH_MEAN_C

# global
needRedraw = False  # теоретически можно использовать для отложенной перерисовки окон


def on_t_change(value):
    if not needRedraw:
        return
    tl = cv.getTrackbarPos('T Lower', 'marking')
    th = cv.getTrackbarPos('T Higher', 'marking')
    # TODO похоже здесь утечка памяти, но для тестирования библиотеки это не критично
    _, grayThreshold = cv.threshold(grayImg, tl, th, cv.THRESH_BINARY)
    cv.imshow('threshold image', grayThreshold)


def on_a_change(value):
    if not needRedraw:
        return
    am = cv.getTrackbarPos('A maxValue', 'marking')
    ab = cv.getTrackbarPos('A blockSize', 'marking')
    ab_prepared = ab * 2 + 1  # размер блока должен быть нечётным числом > 0
    c = cv.getTrackbarPos('A C', 'marking')
    c_float = c / 25.0  # нормализация
    print(c_float)
    # TODO похоже здесь утечка памяти, но для тестирования библиотеки это не критично
    adaptive = cv.adaptiveThreshold(grayImg, am, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, ab_prepared, c_float);
    cv.imshow('adaptive Gauss', adaptive)
    adaptive = cv.adaptiveThreshold(grayImg, am, cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY, ab_prepared, c_float);
    cv.imshow('adaptive mean', adaptive)


img = cv.imread(FILE_NAME)


grayImg = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

cv.namedWindow('marking', cv.WINDOW_NORMAL)

cv.createTrackbar('T Lower', 'marking', 10, 255, on_t_change)
cv.createTrackbar('T Higher', 'marking', 170, 255, on_t_change)
cv.createTrackbar('A maxValue', 'marking', 255, 255, on_a_change)
cv.createTrackbar('A blockSize', 'marking', 57, 255, on_a_change)
cv.createTrackbar('A C', 'marking', 50, 255, on_a_change)

# с resize чудеса какие-то, но работает и ладно
cv.resizeWindow('marking', 400, 200)

cv.imshow('img', img)

# костыль, а что поделать, если напрямую нельзя указать размер окна.
# im = cv.imread(FILE_NAME)                    # Read image
# imS = cv.resize(im, (10, 10))                # Resize image
# cv.imshow('marking', imS)

# включаем перерисовку
needRedraw = True
# рисуем один раз при запуске
on_t_change(-1)
on_a_change(-1)

cv.waitKey(0)

cv.destroyAllWindows()

