import cv2 as cv


def block_size_step():
    global curr_iterables
    global curr_block_size
    curr_iterables += 1
    if curr_iterables > DELAY_FRAME:
        curr_iterables = 0
        return
    curr_block_size += D_BLOCK_SIZE
    if curr_block_size > MAX_BLOCK_SIZE:
        curr_block_size = MIN_BLOCK_SIZE


# settings
MIN_BLOCK_SIZE, MAX_BLOCK_SIZE  = 11, 121   # минимальный/максимальный размер блока,
                                            # можно рассчитать от разрешения камеры, должен быть нечетным
D_BLOCK_SIZE = 2  # изменение размера болка, должно быть четным
DELAY_FRAME = 10  # обновляем раз в DELAY_FRAME кадров, можно завязать на миллисекунды при желании

# global variables
curr_block_size = MIN_BLOCK_SIZE
curr_iterables = 0

# видео захват с устройства по умолчанию
cap = cv.VideoCapture(0)

while 1:
    # читаем кадр
    _, frame = cap.read()

    # в серый
    grayImg = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

    # гаусс
    gauss = cv.adaptiveThreshold(grayImg, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, curr_block_size, 2);

    # imshow block отображает три окна, исходный / чернобелый / после адаптивного преобразования
    cv.imshow('frame', frame)
    cv.imshow('grayImg', grayImg)
    cv.imshow('gauss', gauss)

    # изменяем размер блока для адаптивного поиска порога
    block_size_step()

    # выход по Escape
    if cv.waitKey(1) & 0xFF == 27:
        break

# выходим
cv.destroyAllWindows()
cap.release()
